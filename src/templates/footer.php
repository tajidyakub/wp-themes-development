<?php
/**
 * Theme's footer template.
 *
 * @package wpsandbox
 * @since 1.0.0
 */
?>
	<footer>
		<?php get_template_part( 'partials/footer', 'copyright' );?>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>
