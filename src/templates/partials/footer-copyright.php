<div class="container max-in">
	<div class="row">
		<div class="col py-4 px-md-5 ">
			<p class="text-center text-muted">
				<?php the_field( 'footer_copyright_text', 'options'); ?>
			</p>
		</div>
	</div>
</div>
